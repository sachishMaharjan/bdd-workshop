package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.List;

public class MyStepdefs {


    @After
    public void herePutAGivenAfter() {
        System.out.println("After");
    }

    @Before
    public void herePutAGivenBefore() {
        System.out.println("Before");
    }

    @Given("here put a given")
    public void herePutAGiven() {
        System.out.println("Here go the given");
    }

    @When("I add a when step")
    public void iAddAWhenStep() {
        System.out.println("Here go the when");
    }

    @Then("I have to assert something")
    public void iHaveToAssertSomething() {
        System.out.println("Here go the then");
    }

    @And("another given")
    public void anotherGiven() {
    }

    @When("another given with {string}")
    public void anotherGivenWith(String something) {
        System.out.println(something);
    }

    @And("I want to add this")
    public void iAdd(List<String> data) {
        for (String datum : data) {
            System.out.println(datum);
        }

    }

    @When("I want to add a variable {}")
    public void iWantToAddAVariableVariable(String variable) {
        System.out.println(variable);
    }

    @Given("this is the background")
    public void thisIsTheBackground() {
        System.out.println("background");
    }
}

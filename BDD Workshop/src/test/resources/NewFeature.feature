Feature: Name of the feature

  Background:
    Given this is the background

  Scenario: Name of the scenario
    Given here put a given
    When I add a when step
    Then I have to assert something

  Scenario: Another scenario
    Given I have to assert something
    When another given with "something"
    And another given with "somethingsajskdhjkaskhdjkaskjdhajkhdjaskhdjkask"
    Then I add a when step
    And I want to add this
      | Holanda  |
      | Como vas |

  Scenario Outline: This is a scenario outline
    Given here put a given
    When I want to add a variable <variable>

    Examples:
      | variable |
      | dog      |
      | cat      |

